"use strict";

function TodoNityApp() {
    var version = "v2.0.0",

        appStorage = new AppStorage("todoNity"),
        taskListManager = new TaskListManager(),

        //? move to actual creation
        taskList = new TaskList(),

        // taskList = new TaskList(),
        //get nextTaskId & nextTaskListId from appStorage (set to 1 if no value exists)
        //? move textTaskId to taskList and nextTaskListId to taskListManager?
        nextTaskId = appStorage.getValue("nextTaskId") || 1,
        nextTaskListId = appStorage.getValue("nextTaskListId") || 1;

    this.start = function () {
        loadTaskListManager();
        gotoTaskOverview();
        $("#app>header").append(" " + version);
    };

    function getUniqueTaskId(task) {
        task.id = nextTaskId++;
    }

    function getUniqueTaskListId(taskList) {
        taskList.id = nextTaskListId++;
    }

    //add taskList to global var and taskListManager
    function addTaskList(taskListName, anonymous) {
        //ToDo
        //add task via taskListOverview (#new-task-name -> enter key pressed)
        if (typeof taskListName == 'undefined') {
            const taskListName = $("#new-task-list-name").val();
            //? if (taskName) vs typeoff != undefined
            if (taskListName) {

                taskList = new TaskList([], taskListName);
                getUniqueTaskListId(taskList);
                taskListManager.addTaskList(taskList)
            }
        } else {

        }
    }

    //add taskList to DOM
    function showTaskList(taskListName) {
        //ToDo
    }

    //add task to storage&DOM or just add task to storage (called with taskName argument)
    function addTask(taskName, taskDescription, taskPriority) {
        //add task via taskOverview (#new-task-name -> enter key pressed)
        if (typeof taskName == 'undefined') {
            const taskName = $("#new-task-name").val();
            if (taskName) {
                var task = new Task(taskName);
                getUniqueTaskId(task);
                taskList.addTask(task);
                taskListManager.refreshActiveTaskList(taskList);
                appStorage.setValue("nextTaskId", nextTaskId);
                addTaskElement(task, true);
                saveTaskListManager();
                //Reset the text field and move focus back on it
                $("#new-task-name").val("").focus();
            }
        }
        //add task via taskNewView (add-task-btn)
        else {
            if (taskName) {
                var task = new Task(taskName, taskDescription, taskPriority);
                getUniqueTaskId(task);
                taskList.addTask(task);
                taskListManager.refreshActiveTaskList(taskList);
                appStorage.setValue("nextTaskId", nextTaskId);
                saveTaskListManager();
            }
        }
    }

    //add task to DOM
    function addTaskElement(task, animate) {
        var $task = $("#task-template .task").clone();
        $task.data("task-id", task.id);
        $("div.task-name", $task).text(task.name);

        //add priority images to tasks in overview
        switch (task.priority) {
            case 1:
                $("div.task-priority", $task).append("<img src = './images/prio_low.png' />");
                break;
            case 2:
                $("div.task-priority", $task).append("<img src = './images/prio_medium.png' />");
                break;
            case 3:
                $("div.task-priority", $task).append("<img src = './images/prio_high.png' />");
                break;
        }

        //add checked/unchecked images to tasks in overview
        switch (task.status) {
            case 2:
                $("div.task-status", $task).append("<button type='button' class='checkbox unchecked'><img src='./images/taskUnchecked.png' /></button>");
                if (animate) {
                    $("#task-list").append($task).hide().slideDown('slow');
                } else {
                    $("#task-list").append($task);
                }
                break;
            case 3:
                $("div.task-status", $task).append("<button type='button' class='checkbox checked'><img src='./images/taskChecked.png' /></button>");
                if (animate) {
                    $("#task-list-done").append($task).hide().slideDown('slow');
                } else {
                    $("#task-list-done").append($task);
                }
                break;
        }

        //Overview: click task div event handler
        $("#taskNameDiv", $task).click(function () {
            gotoTaskEditView(task);
        });

        //Overview: check task button event handler
        $("button.checkbox", $task).click(function () {
            checkToggleTask(task);
            saveTaskListManager();
            loadTaskList();
            rebuildTaskList();
        });
    }

    //navigate to taskOverview (DOM rebuild, init event handlers, sort&rebuild taskList)
    function gotoTaskListOverview() {
        const $taskListOverview = $("#taskListOverview-template #overview-container").clone();
        clearMain();
        $("#main").append($taskListOverview);

        //todo
        // //Event handler for inputs in search field
        // $("#filter-task-name").on('input', function (e) {
        //     filterTaskList(e.target.value);
        //     if (taskList.isEmpty()) {
        //         rebuildTaskList();
        //         errorBox("Nothing was found - recheck your input.")
        //     } else {
        //         rebuildTaskList();
        //     }
        // });

        //todo
        // //Event handler for hitting Enter-Btn in new-task inputField
        // $("#new-task-name").keypress(function (e) {
        //     if (e.which == 13) // Enter key
        //     {
        //         addTask();
        //         loadTaskList();
        //         rebuildTaskList();
        //         return false;
        //     }
        // });

        //Overview: add task button event handler
        $("button#add-task-list-btn").click(function () {
            // todo
            gotoTaskOverview();
            $("#new-task-list-name").focus();

        });

        $("button#goto-task-overview-btn").click(function () {
            gotoTaskOverview();
        });

        //TODO: rewrite/adjust functions
        loadTaskList();
        rebuildTaskList();
    }

    //navigate to taskOverview (DOM rebuild, init event handlers, sort&rebuild taskList)
    function gotoTaskOverview() {
        const $taskOverview = $("#taskOverview-template #overview-container").clone();
        clearMain();
        $("#main").append($taskOverview);

        //Event handler for inputs in search field
        $("#filter-task-name").on('input', function (e) {

            filterTaskList(e.target.value);
            if (taskList.isEmpty()) {
                rebuildTaskList();
                errorBox("Nothing was found - recheck your input.")
            } else {
                rebuildTaskList();
            }
        });

        //Event handler for hitting Enter-Btn in new-task inputField
        $("#new-task-name").keypress(function (e) {
            if (e.which == 13) // Enter key
            {
                addTask();
                loadTaskList();
                rebuildTaskList();
                resetTextareaHeight();
                return false;
            }
        });

        //Event handler for hitting Enter-Btn in new-task inputField
        $("#new-task-list-name").on('input', function (e) {
            $(function () {
                const $taskListName = document.querySelector("#main #new-task-list-name");
                // const taskListName = document.querySelector("#main #new-task-list-name").val();
                const taskListName = $("#main #new-task-list-name").val();
                if (document.querySelector("#main #new-task-list-name").validity.valid) {
                    if (e.which == 13) {
                        console.log(taskList);
                        addTaskList()
                    }
                    console.log($taskListName);
                    console.log(taskListName);
                    console.log("valid task list name");
                } else {
                    alert("invalid task list name");
                }

            });
        });

        //Overview: add task button event handler
        $("button#add-task-btn").click(function () {
            gotoTaskNewView();
        });

        $("button#goto-task-list-overview-btn").click(function () {
            gotoTaskListOverview();
        });

        loadTaskList();
        rebuildTaskList();

        //move focus to #new-task-name field
        $("#new-task-name").focus();
    }

    //navigate to taskEditView (DOM rebuild, populate details fields, init event handlers)
    function gotoTaskEditView(task) {
        const $taskEditView = $("#taskEditView-template .details").clone();
        clearMain();
        $("#main").append($taskEditView);
        if (task) {
            // Populate all of the details fields
            $("#main .details select, #main .details textarea").each(function () {
                var $input = $(this);
                var fieldName = $input.data("field");
                $input.val(task[fieldName]);
            });

            //TaskNew&EditView: change details-fields event handler
            $("#main .details select, #main .details textarea").on('input', function (e) {
                onInputTaskDetails(task.id, $(this), e);
            });

            //TaskEditView: delete button event handler
            $("button.delete").click(function () {
                removeTask(task);
                gotoTaskOverview();
            });
        }

        //taskNewView: enter or escape key pressed (document level) event handlers
        $(document).on('keyup', function (e) {
            $(function () {
                //?? @erik/lucas - Frage zu s.fn.init(1)
                const $taskDescription = $("#main div.details-task-description textarea").get(0);
                if (e.which === 13 && e.target !== $taskDescription) { //Enter key || Escape key
                    //navigate to overview and remove event handler
                    gotoTaskOverview();
                    $(this).off('keyup');
                    return false;
                } else if (e.which === 27) {
                    //navigate to overview and remove event handler
                    gotoTaskOverview();
                    $(this).off('keyup');
                    return false;
                }
            });
        });

        //TaskEditView: done button event handler
        $("button#done-btn").click(function () {
            gotoTaskOverview();
        })
    }

    //navigate to taskNewView (DOM rebuild, init event handlers)
    function gotoTaskNewView() {
        const $taskNewView = $("#taskEditView-template .details").clone();
        clearMain();
        $("#main").append($taskNewView);

        $(function () {
            $("#main textarea.details-task-name").on('input', function (e) {
                //?? javascript selektor funktioniert.. jquery selektor nicht (zu schnell?)
                // console.log(document.querySelector("#main input.details-task-name"));
                // console.log($("#main input.details-task-name"));

                if (document.querySelector("#main textarea.details-task-name").validity.valid) {
                    $("#main #details-task-name-error").hide();
                    enableDoneBtn();
                } else {
                    $("#main #details-task-name-error").show();
                    disableDoneBtn();
                }
            });
        });

        $("#cancel-btn").show();
        disableDoneBtn();

        //TaskNew&EditView: done button event handler
        $("button#done-btn").click(function (e) {
            addNewTask();
        });

        //TaskNewView: cancel button event handler
        $("button#cancel-btn").click(function () {
            gotoTaskOverview();
        });

        //taskNewView: enter or escape key pressed (document level) event handlers
        $(document).on('keyup', function (e) {

            const $taskDescription = $("#main div.details-task-description textarea").get(0);
            const $taskName = $("#main textarea.details-task-name").get(0);

            if (e.which === 13 && e.target !== $taskDescription && $taskName.validity.valid) {
                //save task, navigate to overview and remove event handler
                addNewTask();
                gotoTaskOverview();
                $(this).off('keyup');
                return false;
            } else if (e.which === 27) {
                //navigate to overview and remove event handler
                gotoTaskOverview();
                $(this).off('keyup');
                return false;
            }
        });

        function addNewTask() {
            const name = $("#main textarea.details-task-name").val();
            const description = $("#main div.details-task-description textarea").val();
            const priority = parseInt($("#main div.details-task-priority select").val());

            addTask(name, description, priority);
            gotoTaskOverview();
        }
    }

    //empty #main in DOM
    function clearMain() {
        $("#main").empty();
    }

    //remove task from storage & taskListManager, persist changes in localStorage
    function removeTask(task) {
        taskList.removeTask(task.id);
        taskListManager.refreshActiveTaskList(taskList);
        saveTaskListManager();
    }

    //save taskListManager
    function saveTaskListManager() {
        appStorage.setValue("taskListManager", taskListManager.getTaskLists());
    }

    //save activeTaskList
    function saveActiveTaskList() {
        appStorage.setValue("activeTaskList", taskListManager.getActiveTaskListId);
    }

    //load tasksListManager from storage
    function loadTaskListManager() {
        //get taskLists from storage
        var tasksLists = appStorage.getValue("taskListManager");
        var activeTaskList = appStorage.getValue("activeTaskList");
        if (activeTaskList == null) {}
        //sort tasks
        //Todo: sort
        taskListManager = new TaskListManager(tasksLists, activeTaskList);
    }

    //load tasks via taskListId from taskListManager
    function loadTaskList(taskListId) {
        const tasks = [];
        if (typeof taskListId !== 'undefined') {
            //get tasks
            tasks = taskListManager.getTaskList(taskListId);
            //sort tasks
            //Todo: typeoff
            if (tasks) sortTasks(tasks);
            taskList = new TaskList(tasks);
        }
    }

    //rebuild taskList in DOM
    function rebuildTaskList() {
        // Remove any old task elements
        $("#task-list").empty();
        $("#task-list-done").empty();
        // Create DOM elements for each task
        taskList.each(function (task) {
            addTaskElement(task, false);
        });
    }

    //TaskEditView: details form input event handler
    function onInputTaskDetails(taskId, $input, e) {
        var task = taskList.getTask(taskId);
        if (task) {
            // get new value from input/select element, write to corresponding 
            // task.field and save changes to storage
            var fieldName = $input.data("field");

            //enable done button and hide validation errors
            enableDoneBtn();
            $("#details-task-name-error").hide();

            if (!e.target.checkValidity()) {
                // if any field doesn't pass the validity-check: disable done button
                disableDoneBtn();
                if (fieldName === "name") {
                    $("#details-task-name-error").show();
                }
            } else {
                if (fieldName === "priority") task[fieldName] = parseInt($input.val());
                else task[fieldName] = $input.val();
                saveTaskListManager();
            }
        }
    }

    //sort tasks by priority>name
    function sortTasks(tasks) {
        //sort by task.name 
        tasks.sort(function (a, b) {
            var nameA = a.name.toUpperCase();
            var nameB = b.name.toUpperCase();
            if (nameA < nameB) return -1;
            if (nameA > nameB) return 1;
            return 0;
        })
        //sort presorted list by priorities (ascending)
        tasks.sort(function (a, b) {
            return b.priority - a.priority;
        })
    }

    //filter displayed tasks by search string
    function filterTaskList(searchStr) {
        //load taskList from storage
        loadTaskList();
        //save tasksList.tasks and create a new empty instance of taskList
        const tasks = taskList.getTasks();
        taskList = new TaskList();
        tasks.forEach(task => {
            if (task.name.includes(searchStr))
                taskList.addTask(task)
        });
    }

    //toggle task status (checked<->unchecked)
    function checkToggleTask(task) {
        if (task.status === 2) task.status = 3;
        else task.status = 2;
    }

    //taskNew&EditView: enable done button
    function enableDoneBtn() {
        $("#done-btn").prop("disabled", false);
        $("#done-btn").prop('title', 'Return to overview');
    }

    //taskNew&EditView: enable done button
    function disableDoneBtn() {
        $("#done-btn").prop("disabled", true);
        $("#done-btn").prop('title', 'Enter the required Information');
    }

    function errorBox(msg) {
        var $task = $("#task-template .task").clone();
        $("div.task-name", $task).text(msg);

        //add warning image
        $("div.task-priority", $task).append("<img src = './images/warning.png' />");

        //remove unnecessary elements
        // $task.find(".tools").remove();
        $task.find(".task-status").remove();

        //?? warum geht das so nicht?
        // $($task).remove(".tools");
        // $("li.task", $task).remove(".task-status");

        //display errorBox and add class for css
        $("#task-list").append($task).hide().slideDown('fast');
        $("#task-list li.task").addClass("errorBox")
    }

    /* function auto_grow(textarea) {
        element.style.height = "auto";
        element.style.height = (element.scrollHeight)+"px";
    } */

    function resetTextareaHeight() {
        $("#new-task-name").css("height", "auto");
    }


}

//start app
$(function () {
    window.app = new TodoNityApp();
    window.app.start();
});