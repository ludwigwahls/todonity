//wrapper class for localStorage object
function AppStorage(appName) {
    //prefix all keys with 'appName.' to avoid collisions in browsers localStorage
    var prefix = (appName ? appName + "." : "");

    //checks if localStorage object exists
    this.localStorageSupported = (('localStorage' in window) &&
        window['localStorage']);

    //put key-value-pair in localStorage (can only hold strings)
    this.setValue = function (key, val) {
        if (this.localStorageSupported)
            localStorage.setItem(prefix + key, JSON.stringify(val));
        return this;
    };

    //get key-value-pair out of localStorage (parsing the string to value before returning)
    this.getValue = function (key) {
        if (this.localStorageSupported)
            return JSON.parse(localStorage.getItem(prefix + key));
        else return null;
    };

    //remove key-value-pair from localStorage
    this.removeValue = function (key) {
        if (this.localStorageSupported)
            localStorage.removeItem(prefix + key);
        return this;
    };

    //remove all key-value-pairs of our app from localStorage
    this.removeAll = function () {
        var keys = this.getKeys();
        for (var i in keys) {
            this.remove(keys[i]);
        }
        return this;
    };

    this.getKeys = function (filter) {
        var keys = [];
        if (this.localStorageSupported) {
            for (var key in localStorage) {
                if (isAppKey(key)) {
                    // Remove the prefix from the key
                    // ?? syntax
                    if (prefix) key = key.slice(prefix.length);
                    // Check the filter
                    if (!filter || filter(key)) {
                        keys.push(key);
                    }
                }
            }
        }
        return keys;
    };

    function isAppKey(key) {
        //??syntax
        if (prefix) {
            return key.indexOf(prefix) === 0;
        }
        return true;
    };

    this.contains = function (key) {
        return this.get(key) !== null;
    };
}