function Task(name, description, priority, taskListId) {
    this.name = name || undefined;
    this.description = description || undefined;
    this.priority = priority || Task.priorities.normal;

    this.taskListId = taskListId || undefined;

    this.id = undefined;
    this.status = Task.states.started;


    // disabled
    // this.startDate = null;
    // this.pctComplete = 0;
    // this.created = new Date();
    // this.dueDate = null;
}
// Define some "static variables" on the Task object

// Task.nextTaskId = 1;

Task.priorities = {
    none: 0,
    low: 1,
    normal: 2,
    high: 3
};
Task.states = {
    none: 0,
    notStarted: 1,
    started: 2,
    completed: 3
};

class TaskListb {

    constructor(id) {
        this.id = id;
        this.tasks = [];
    }

    setTasks(newTasks) {
        this.tasks = newTasks;
    }

    getTasks() {
        return this.tasks;
    }
}






function TaskList(tasks, name) {
    //? warum hier nicht this.tasks
    tasks = tasks || [];

    this.name = name || undefined;

    //! always required?
    this.id = undefined;

    this.setId = function (newId) {
        this.id = newId;
    };

    this.getId = function () {
        return this.id;
    }

    this.getTasks = function () {
        return tasks;
    };

    this.addTask = function (task) {
        tasks.push(task);
        return this;
    };

    this.removeTask = function (taskId) {
        var i = getTaskIndex(taskId);
        if (i >= 0) {
            var task = tasks[i];
            tasks.splice(i, 1);
            return task;
        }
        return null;
    };

    function getTaskIndex(taskId) {
        for (var i in tasks) {
            if (tasks[i].id == taskId) {
                return parseInt(i);
            }
        }
        // Not found
        return -1;
    };

    this.isEmpty = function () {
        if (tasks.length === 0) return true;
        else return false;
    };

    this.getTask = function (taskId) {
        var index = getTaskIndex(taskId);
        return (index >= 0 ? tasks[index] : null);
    };

    this.each = function (callback) {
        for (var i in tasks) {
            callback(tasks[i]);
        }
    };
}

function TaskListManager(taskLists, activeTlId) {
    taskLists = taskLists || [];

    // activeTaskList = Id
    this.activeTaskListId = activeTlId || 'undefined';

    //sets a taskList specified by id to active
    this.setActiveTaskList = function (newActiveTaskListId) {
        if (Number.isInteger(newActiveTaskListId)) {
            this.activeTaskListId = newActiveTaskListId;
        }
    }

    //remove activeTaskList from taskListManager and add taskList from global var
    this.refreshActiveTaskList = function (newTaskList) {
        //remove activeTaskList from taskListManager
        this.removeTaskList(this.getActiveTaskListId);
        this.addTaskList(newTaskList);
        return this;
    }

    // this.hasMultipleTaskLists = function() {
    //     if (taskLists.length > 1) return true;
    //     else return false;
    // }

    //returns the active taskList
    this.getActiveTaskListId = function () {
        return this.activeTaskListId;
    }

    //returns the active taskList
    this.getActiveTaskList = function () {
        return this.getTaskList(activeTaskListId);
    }

    this.getTaskLists = function () {
        return taskLists;
    };

    this.addTaskList = function (taskList) {
        taskLists.push(taskList);
        return this;
    };

    this.removeTaskList = function (taskListId) {
        var i = getTaskListIndex(taskListId);
        if (i >= 0) {
            var taskList = taskLists[i];
            taskLists.splice(i, 1);
            //? wozu
            return taskList;
        }
        return null;
    };

    function getTaskListIndex(taskListId) {
        for (var i in taskLists) {
            if (taskLists[i].id == taskListId) {
                return parseInt(i);
            }
        }
        // Not found
        return -1;
    };

    this.isEmpty = function () {
        if (taskLists.length === 0) return true;
        else return false;
    };

    this.getTaskList = function (taskListId) {
        var index = getTaskListIndex(taskListId);
        return (index >= 0 ? taskLists[index] : null);
    };

    this.each = function (callback) {
        for (var i in taskLists) {
            callback(taskLists[i]);
        }
    };
}